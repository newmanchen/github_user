package com.newman.githubusers.model.data

import com.google.gson.annotations.SerializedName
import com.newman.githubusers.ui.adapter.SearchUserAdapter

/**
 * login: "newman",
id: 3926915,
node_id: "MDQ6VXNlcjM5MjY5MTU=",
avatar_url: "https://avatars3.githubusercontent.com/u/3926915?v=4",
gravatar_id: "",
url: "https://api.github.com/users/newman",
html_url: "https://github.com/newman",
followers_url: "https://api.github.com/users/newman/followers",
following_url: "https://api.github.com/users/newman/following{/other_user}",
gists_url: "https://api.github.com/users/newman/gists{/gist_id}",
starred_url: "https://api.github.com/users/newman/starred{/owner}{/repo}",
subscriptions_url: "https://api.github.com/users/newman/subscriptions",
organizations_url: "https://api.github.com/users/newman/orgs",
repos_url: "https://api.github.com/users/newman/repos",
events_url: "https://api.github.com/users/newman/events{/privacy}",
received_events_url: "https://api.github.com/users/newman/received_events",
type: "User",
site_admin: false,
score: 1.0
 */
data class User(
    @SerializedName("login") val login: String,
    @SerializedName("id") val id: Int,
    @SerializedName("node_id") val node_id: String,
    @SerializedName("avatar_url") val avatar_url: String,
    @SerializedName("gravatar_id") val gravatar_id: String,
    @SerializedName("url") val url: String,
    @SerializedName("html_url") val html_url: String,
    @SerializedName("followers_url") val followers_url: String,
    @SerializedName("following_url") val following_url: String,
    @SerializedName("gists_url") val gists_url: String,
    @SerializedName("starred_url") val starred_url: String,
    @SerializedName("subscriptions_url") val subscriptions_url: String,
    @SerializedName("organizations_url") val organizations_url: String,
    @SerializedName("repos_url") val repos_url: String,
    @SerializedName("events_url") val events_url: String,
    @SerializedName("received_events_url") val received_events_url: String,
    @SerializedName("type") val type: String,
    @SerializedName("site_admin") val site_admin: Boolean,
    @SerializedName("score") val score: Float,

    var style: Int?
)