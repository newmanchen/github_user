package com.newman.githubusers

import android.app.Activity
import android.app.Application.ActivityLifecycleCallbacks
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDexApplication
import com.facebook.cache.disk.DiskCacheConfig
import com.facebook.common.memory.MemoryTrimType
import com.facebook.common.memory.MemoryTrimmableRegistry
import com.facebook.common.memory.NoOpMemoryTrimmableRegistry
import com.facebook.common.util.ByteConstants
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.core.ImagePipelineFactory
import com.facebook.stetho.Stetho
import com.newman.githubusers.common.util.MyLog
import com.newman.githubusers.di.components.AppComponent
import com.newman.githubusers.di.tool.AppInjector
import com.newman.githubusers.network.manager.github.GithubCenter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class MainApplication : MultiDexApplication(), ActivityLifecycleCallbacks, HasActivityInjector {
    companion object {
        private val TAG = MainApplication::class.java.simpleName
        lateinit var app: MainApplication
            private set

        fun getStr(@StringRes res: Int): String {
            return try {
                app.getString(res)
            } catch (e: Exception) {
                ""
            }
        }
    }

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>
    lateinit var appComponent: AppComponent
        private set
    var mainActivity: AppCompatActivity? = null
    var appVersionName: String? = null
        private set
    var appVersionCode = 0
        private set
    var appName: String? = null
        private set

    var githubCenter: GithubCenter = GithubCenter()

    override fun onCreate() {
        super.onCreate()
        appComponent = AppInjector.init(this)
        app = this
        initApplication()
        ProcessLifecycleOwner.get().lifecycle.addObserver(AppLifecycleOwner())
    }

    private fun initApplication() {
        appName = getStr(R.string.app_name)
        try {
            appVersionName = packageManager.getPackageInfo(packageName, 0).versionName
            appVersionCode = packageManager.getPackageInfo(packageName, 0).versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            MyLog.e(TAG, "get version name fail", e)
        }
        initFresco()
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        } else {
            //FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        }
    }

    private fun initFresco() {
        // low memory notify config
        val memoryTrimmableRegistry: MemoryTrimmableRegistry =
            NoOpMemoryTrimmableRegistry.getInstance()
        memoryTrimmableRegistry.registerMemoryTrimmable { trimType: MemoryTrimType ->
            val suggestedTrimRatio = trimType.suggestedTrimRatio
            if (MemoryTrimType.OnCloseToDalvikHeapLimit.suggestedTrimRatio == suggestedTrimRatio || MemoryTrimType.OnSystemLowMemoryWhileAppInBackground.suggestedTrimRatio == suggestedTrimRatio || MemoryTrimType.OnSystemLowMemoryWhileAppInForeground.suggestedTrimRatio == suggestedTrimRatio
            ) {
                ImagePipelineFactory.getInstance().imagePipeline.clearMemoryCaches()
            }
        }
        // disk config
        val diskCacheConfig = DiskCacheConfig.newBuilder(this)
        DiskCacheConfig.newBuilder(this)
            .setBaseDirectoryName("fresco_" + getString(R.string.app_name) + "cache") // cache folder name
            .setMaxCacheSize(100 * ByteConstants.MB.toLong()) // cached size
        val builder = ImagePipelineConfig.newBuilder(this)
        builder.setDownsampleEnabled(true)
            .setMemoryTrimmableRegistry(memoryTrimmableRegistry)
            .setMainDiskCacheConfig(diskCacheConfig.build())
        Fresco.initialize(this, builder.build())
    }

    fun showKeyboardAtView(target: View?) {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.showSoftInput(target, InputMethodManager.SHOW_IMPLICIT)
    }

    fun hideKeyboard(focusView: View?) {
        focusView?.let {
            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(focusView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle) {}
    override fun onActivityStarted(activity: Activity) {}
    override fun onActivityResumed(activity: Activity) {}
    override fun onActivityPaused(activity: Activity) {}
    override fun onActivityStopped(activity: Activity) {}
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
    override fun onActivityDestroyed(activity: Activity) {}
    
    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
}