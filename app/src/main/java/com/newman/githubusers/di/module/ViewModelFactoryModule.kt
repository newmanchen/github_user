package com.newman.githubusers.di.module

import androidx.lifecycle.ViewModelProvider
import com.newman.githubusers.di.tool.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}