package com.newman.githubusers.di.module.ui

import androidx.lifecycle.ViewModel
import com.newman.githubusers.di.annotations.ViewModelKey
import com.newman.githubusers.ui.SearchUserVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SearchUserViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(SearchUserVM::class)
    abstract fun bindSearchUserVM(userViewModel: SearchUserVM): ViewModel
}