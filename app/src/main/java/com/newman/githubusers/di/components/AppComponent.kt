package com.newman.githubusers.di.components

import com.newman.githubusers.MainApplication
import com.newman.githubusers.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class
    ]
)
interface AppComponent : MainComponent<MainApplication> {
    @Component.Factory
    interface Factory {
        fun newAppComponent(
            @BindsInstance application: MainApplication
        ): AppComponent
    }
}