package com.newman.githubusers.di.tool

import android.app.Application
import androidx.lifecycle.ViewModel

import androidx.lifecycle.ViewModelProvider
import com.newman.githubusers.ui.SearchUserVM

class MyViewModelFactory(param: String) :
    ViewModelProvider.Factory {
    private val mParam: String = param

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchUserVM(mParam) as T
    }
}