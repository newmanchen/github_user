package com.newman.githubusers.di.module

import com.newman.githubusers.network.manager.github.service.GithubApi
import com.newman.githubusers.repository.GithubUserRepository
import com.newman.githubusers.repository.UserInMemoryByItemRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RepositoryModule {
    @Binds
    abstract fun provideGithubUserRepository(userInMemoryByItemRepository: UserInMemoryByItemRepository): GithubUserRepository

    @Binds
    abstract fun provideGithubApi(githubApi: GithubApi): GithubApi

    @Module
    companion object {
        @Provides
        fun provideUserInMemoryByItemRepository(): UserInMemoryByItemRepository? {
            return UserInMemoryByItemRepository()
        }
    }
}