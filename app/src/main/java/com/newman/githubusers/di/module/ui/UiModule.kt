package com.newman.githubusers.di.module.ui

import com.newman.githubusers.di.annotations.ActivityScope
import com.newman.githubusers.di.module.ViewModelFactoryModule
import com.newman.githubusers.ui.SearchUserActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UiModule {
//    @ActivityScope
//    @ContributesAndroidInjector(
//        modules = [
//            SearchUserViewModelsModule::class,
//            ViewModelFactoryModule::class
//        ]
//    )
//    abstract fun bindSearchUserActivity(): SearchUserActivity
}