package com.newman.githubusers.di.components

import dagger.android.AndroidInjector

interface MainComponent<T> : AndroidInjector<T> {
    override fun inject(application: T)
}