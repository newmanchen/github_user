package com.newman.githubusers.di.annotations

import androidx.lifecycle.ViewModel
import dagger.MapKey
import javax.inject.Qualifier
import javax.inject.Scope
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass

// region qualifiers
@Qualifier
@Retention(RUNTIME)
annotation class ActivityContext

@Qualifier
@Retention(RUNTIME)
annotation class ApplicationContext

@Qualifier
@Retention(RUNTIME)
annotation class ApiBaseUrl
// endregion

// region scope
@Scope
@Retention(RUNTIME)
annotation class ActivityScope

@Scope
@Retention(RUNTIME)
annotation class FragmentScope

@Scope
@Retention(RUNTIME)
annotation class ServiceScope
// endregion

// region document
@MustBeDocumented
@Target(
    FUNCTION,
    PROPERTY_GETTER,
    PROPERTY_SETTER
)
@Retention(RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)
// endregion