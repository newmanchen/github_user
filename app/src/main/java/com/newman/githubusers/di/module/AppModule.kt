package com.newman.githubusers.di.module

import android.app.Application
import android.content.Context
import com.newman.githubusers.MainApplication
import com.newman.githubusers.di.annotations.ApplicationContext
import com.newman.githubusers.di.module.ui.UiModule
import dagger.Binds
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule

@Module(
    includes = [
        RepositoryModule::class,
        AndroidSupportInjectionModule::class,
        UiModule::class
    ]
)
abstract class AppModule {

    @Binds
    @ApplicationContext
    abstract fun bindApplicationContext(application: MainApplication): Context

    @Binds
    abstract fun bindApplication(application: MainApplication): Application
}