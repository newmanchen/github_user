package com.newman.githubusers.ui.widget

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.newman.githubusers.R
import com.newman.githubusers.model.data.User

abstract class BaseUserVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    protected val avatar: SimpleDraweeView = itemView.findViewById(R.id.user_avatar)
    protected val nameTv: AppCompatTextView = itemView.findViewById(R.id.user_name)

    open fun bind(user: User, onItemClickListener: (View, Int) -> Unit) {
    }
}