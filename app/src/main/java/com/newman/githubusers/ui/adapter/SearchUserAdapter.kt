/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.newman.githubusers.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.newman.githubusers.R
import com.newman.githubusers.model.data.User
import com.newman.githubusers.repository.list.NetworkState
import com.newman.githubusers.ui.widget.UserLargeVH
import com.newman.githubusers.ui.widget.UserMediumVH
import com.newman.githubusers.ui.widget.UserSmallVH
import kotlin.random.Random

class SearchUserAdapter(
    private val context: Context,
    private val retryCallback: () -> Unit,
    private val onItemClickListener: (View, Int) -> Unit
) : PagedListAdapter<User, RecyclerView.ViewHolder>(USER_COMPARATOR) {
    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            when (getItemViewType(position)) {
                ITEM_TYPE_LARGE -> {
                    (holder as UserLargeVH).bind(it, onItemClickListener)
                }
                ITEM_TYPE_MEDIUM -> {
                    (holder as UserMediumVH).bind(it, onItemClickListener)
                }
                ITEM_TYPE_SMALL -> {
                    (holder as UserSmallVH).bind(it, onItemClickListener)
                }
//            ITEM_TYPE_NETWORK_STATE -> (holder as NetworkStateItemViewHolder).bindTo(networkState)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_TYPE_LARGE -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.widget_vh_user_card_large, parent, false)
                UserLargeVH(itemView)
            }
            ITEM_TYPE_MEDIUM -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.widget_vh_user_card_medium, parent, false)
                UserMediumVH(itemView)
            }
            ITEM_TYPE_SMALL -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.widget_vh_user_card_small, parent, false)
                UserSmallVH(itemView)
            }
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemViewType(position: Int): Int {
        val temp = getItem(position)
        temp?.style?.let {
            return it // if has value, return it
        }
        if (position > 0) {
            val lastUser = getItem(position - 1)
            lastUser?.style?.let {
                if (it == ITEM_TYPE_SMALL) {
                    return ITEM_TYPE_SMALL
                }
            }
        }
        val viewType = when (Random.nextInt(0, 3)) {
            0 -> ITEM_TYPE_LARGE
            1 -> ITEM_TYPE_MEDIUM
            2 -> ITEM_TYPE_SMALL
            else -> {
                ITEM_TYPE_MEDIUM
            }
        }
        temp?.let {
            temp.style = viewType
        }
        return viewType
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    companion object {
        const val ITEM_TYPE_LARGE = 1000 // 2x2
        const val ITEM_TYPE_MEDIUM = 1001 // 2x1
        const val ITEM_TYPE_SMALL = 1002 // 1x1

        val USER_COMPARATOR = object : DiffUtil.ItemCallback<User>() {
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem.id == newItem.id
        }
    }
}