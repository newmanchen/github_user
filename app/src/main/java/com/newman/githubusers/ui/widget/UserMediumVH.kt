package com.newman.githubusers.ui.widget

import android.view.View
import com.newman.githubusers.model.data.User

class UserMediumVH(itemView: View) : BaseUserVH(itemView) {

    override fun bind(user: User, onItemClickListener: (View, Int) -> Unit) {
        avatar.setImageURI(user.avatar_url)
        nameTv.text = user.login

        itemView.setOnClickListener {
        }
    }
}