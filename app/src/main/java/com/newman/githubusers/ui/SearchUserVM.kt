package com.newman.githubusers.ui

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MediatorLiveData
import com.newman.githubusers.common.viewmodel.BaseViewModel
import com.newman.githubusers.repository.UserInMemoryByItemRepository

class SearchUserVM constructor(
    private var queryString: String = ""
) : BaseViewModel(), LifecycleObserver {
    private val githubUserRepository: UserInMemoryByItemRepository by lazy {
        UserInMemoryByItemRepository()
    }

    private val searchUserResult by lazy {
        githubUserRepository.users(queryString)
    }

    val users = searchUserResult.pagedList
    val networkState = searchUserResult.networkState
    val refreshState = searchUserResult.refreshState

    val hasAnyUser: MediatorLiveData<Boolean> by lazy {
        MediatorLiveData<Boolean>()
    }

    init {
        hasAnyUser.value = false
        hasAnyUser.addSource(users) {
            hasAnyUser.postValue(it.isNotEmpty())
        }
    }

    fun refreshSearchUsers() {
        searchUserResult.refresh.invoke()
    }

    fun retrySearchUsers() {
        searchUserResult.retry.invoke()
    }

//    fun newSearch(queryString: String) {
//        this.queryString = queryString
//    }
}