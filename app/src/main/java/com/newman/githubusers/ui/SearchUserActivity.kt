package com.newman.githubusers.ui

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.RecyclerView
import com.newman.githubusers.MainApplication
import com.newman.githubusers.R
import com.newman.githubusers.common.activity.MyBaseActivity
import com.newman.githubusers.common.util.MyLog
import com.newman.githubusers.di.annotations.ActivityScope
import com.newman.githubusers.di.tool.Injectable
import com.newman.githubusers.di.tool.MyViewModelFactory
import com.newman.githubusers.ui.adapter.SearchUserAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@ActivityScope
class SearchUserActivity : MyBaseActivity()/*, Injectable*/ {
    private lateinit var toolbar: Toolbar
    private lateinit var searchUserEt: AppCompatEditText
    private lateinit var searchUserRv: RecyclerView

    private var adapter: SearchUserAdapter? = null

//    @Inject
//    lateinit var viewModelFactory: ViewModelProvider.Factory
    private var viewModel: SearchUserVM? = null
//    private val viewModel by lazy(mode = LazyThreadSafetyMode.NONE) {
//        ViewModelProviders.of(this, viewModelFactory)
//            .get(SearchUserVM::class.java)
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
//        initApiCalls()

        observeViewModel()
    }

    private fun initViews() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        searchUserEt = findViewById(R.id.user_keyword_et)
        searchUserEt.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    updatedQueryStringFromInput()
                    true
                } else {
                    false
                }
            }
            setOnKeyListener { _, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    updatedQueryStringFromInput()
                    true
                } else {
                    false
                }
            }
        }

        searchUserRv = findViewById(R.id.search_result_rv)
        adapter = SearchUserAdapter(
            this@SearchUserActivity,
            retryCallback = { viewModel?.retrySearchUsers() },
            onItemClickListener = { _: View, _: Int ->
                //TODO
            })
        searchUserRv.apply {
            val gridLayoutManager = GridLayoutManager(context, 2)
            gridLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    val type = if (adapter != null) adapter?.getItemViewType(position) else -1
                    return if (type == SearchUserAdapter.ITEM_TYPE_SMALL) {
                        1
                    } else {
                        2
                    }
                }
            }
            layoutManager = gridLayoutManager
            adapter = this@SearchUserActivity.adapter
        }
    }

    private fun observeViewModel(queryString: String = "") {
        viewModelStore.clear()
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(queryString))
            .get(SearchUserVM::class.java)
//        viewModel?.newSearch(queryString)
        viewModel?.users?.observe(this, Observer {
            adapter?.submitList(it)
        })
        viewModel?.networkState?.observe(this, Observer {
            adapter?.setNetworkState(it)
        })
    }

    private fun initApiCalls() {
        CoroutineScope(Dispatchers.IO).launch {
            val users = withContext(Dispatchers.Default) {
                MainApplication.app.githubCenter.searchManager.searchUserSync("r")
            }.items

            MyLog.d(TAG, "users count : ${users?.size} ---- \n $users")
        }
    }

    private fun updatedQueryStringFromInput() {
        searchUserEt.text?.trim().toString().let {
            if (it.isNotEmpty()) {
                adapter?.submitList(null)
                observeViewModel(it)
                MainApplication.app.hideKeyboard(searchUserEt)
            } else {
                showToast(R.string.please_input_search_keyword)
            }
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.menu_main, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        return when (item.itemId) {
//            R.id.action_settings -> true
//            else -> super.onOptionsItemSelected(item)
//        }
//    }
}
