package com.newman.githubusers.common.util

import android.util.Log
import com.newman.githubusers.BuildConfig
import com.newman.githubusers.MainApplication

object MyLog {
    private var m_bDebugFlag = BuildConfig.DEBUG || "internal" == BuildConfig.FLAVOR

    // for security usage, print sensitive data if needed = false
    private var m_bSecurityFlag = BuildConfig.DEBUG || "internal" == BuildConfig.FLAVOR

    private val TAG: String = MainApplication.app.appName ?: "GithubUsers"

    fun e(tag: String, vararg messages: Any?) {
        if (m_bDebugFlag) {
            val sb = StringBuilder(getBracketTag(tag))
            for (msg in messages) {
                sb.append(msg)
            }
            Log.e(TAG, sb.toString())
        }
    }

    fun e(tag: String, msg: String, throwable: Throwable?) {
        if (m_bDebugFlag) {
            Log.e(TAG, getBracketTag(tag) + msg, throwable)
        }
    }

    fun d(tag: String, msg: String) {
        if (m_bDebugFlag) {
            Log.d(TAG, getBracketTag(tag) + msg)
        }
    }

    fun d(tag: String, vararg messages: Any?) {
        if (m_bDebugFlag) {
            val sb = StringBuilder(getBracketTag(tag))
            for (msg in messages) {
                sb.append(msg)
            }
            Log.d(TAG, sb.toString())
        }
    }

    fun w(tag: String, vararg messages: Any?) {
        if (m_bDebugFlag) {
            val sb = StringBuilder(getBracketTag(tag))
            for (msg in messages) {
                sb.append(msg)
            }
            Log.w(TAG, sb.toString())
        }
    }

    fun w(tag: String, message: String, throwable: Throwable?) {
        if (m_bDebugFlag) {
            Log.w(TAG, getBracketTag(tag) + message, throwable)
        }
    }

    fun critical(tag: String, vararg messages: Any?) {
        if (m_bSecurityFlag) {
            val sb = StringBuilder(getBracketTag(tag))
            for (msg in messages) {
                sb.append(msg)
            }
            Log.d(TAG, sb.toString())
        }
    }

    private fun getBracketTag(tag: String): String {
        return "[$tag]"
    }
}