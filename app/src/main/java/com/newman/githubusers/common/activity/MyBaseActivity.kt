package com.newman.githubusers.common.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.PersistableBundle
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.newman.githubusers.MainApplication

abstract class MyBaseActivity : AppCompatActivity() {
    protected val TAG = this.javaClass.simpleName
    private var toast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

        MainApplication.app.mainActivity = this
    }

    override fun onResume() {
        super.onResume()

        MainApplication.app.mainActivity = this
    }

    @SuppressLint("ShowToast")
    fun showToast(@StringRes stringResId: Int) {
        showToast(MainApplication.getStr(stringResId))
    }

    @SuppressLint("ShowToast")
    fun showToast(toastString: String?) {
        runOnMainUIThread(Runnable {
            if (toast == null) {
                toast = Toast.makeText(this@MyBaseActivity, toastString, Toast.LENGTH_SHORT)
            } else {
                toast!!.setText(toastString)
            }
            toast!!.show()
        })
    }

    private fun runOnMainUIThread(runnable: Runnable) {
        if (Looper.getMainLooper()
                .thread === Thread.currentThread()
        ) runnable.run() else {
            val handler = Handler(Looper.getMainLooper())
            handler.post(runnable)
        }
    }
}