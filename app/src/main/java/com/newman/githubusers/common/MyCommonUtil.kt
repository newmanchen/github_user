package com.newman.githubusers.common

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.view.Gravity
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import com.newman.githubusers.MainApplication

object MyCommonUtil {
    fun isNetworkConnect(): Boolean {
        val conMgr =
            MainApplication.app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return conMgr?.activeNetworkInfo?.isConnected ?: false
    }

    @ColorInt
    fun getColor(@ColorRes res: Int): Int? {
        MainApplication.app.mainActivity?.let {
            return ContextCompat.getColor(it, res)
        }
        return null
    }

    fun getColorStateList(@ColorRes res: Int): ColorStateList? {
        MainApplication.app.mainActivity?.let {
            return AppCompatResources.getColorStateList(it, res)
        }
        return null
    }

    fun getDrawable(@DrawableRes res: Int): Drawable? {
        MainApplication.app.mainActivity?.let {
            return AppCompatResources.getDrawable(it, res)
        }
        return null
    }

    fun showToast(msg: String) {
        showToast(msg, Gravity.CENTER, false)
    }

    fun showToast(msg: String, isLong: Boolean) {
        showToast(msg, Gravity.CENTER, isLong)
    }

    fun showToast(msg: String, gravity: Int, isLong: Boolean) {
        MainApplication.app.mainActivity?.let {
            val toast = Toast.makeText(
                it,
                msg,
                if (isLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
            )
            toast.setGravity(gravity, 0, 0)
            toast.show()
        }
    }
}