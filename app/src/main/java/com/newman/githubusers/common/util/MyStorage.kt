package com.newman.githubusers.common.util

import android.content.Context
import android.content.SharedPreferences
import com.newman.githubusers.MainApplication.Companion.app
import org.json.JSONArray
import org.json.JSONException
import java.util.*

class MyStorage {
    private val editorHashMap = HashMap<String, SharedPreferences.Editor?>()

    private fun getPref(file: String): SharedPreferences? {
        var pref: SharedPreferences? = null
        val context: Context = app
        if (null != context) {
            pref = context.getSharedPreferences(file, Context.MODE_PRIVATE)
        }
        return pref
    }

    private fun getEditor(file: String): SharedPreferences.Editor? {
        var editor = editorHashMap[file]
        if (editor == null) {
            val pref = getPref(file)
            if (null != pref) {
                editor = pref.edit()
                editorHashMap[file] = editor
            }
        }
        return editor
    }

    //region setter
    fun putString(key: String?, file: String, value: String?): Boolean {
        var isSuccess = false
        val ed = getEditor(file)
        if (null != ed) {
            ed.putString(key, value)
            isSuccess = ed.commit()
        }
        return isSuccess
    }

    fun putLong(key: String?, file: String, value: Long): Boolean {
        var isSuccess = false
        val ed = getEditor(file)
        if (null != ed) {
            ed.putLong(key, value)
            isSuccess = ed.commit()
        }
        return isSuccess
    }

    fun putInt(key: String?, file: String, value: Int): Boolean {
        var isSuccess = false
        val ed = getEditor(file)
        if (null != ed) {
            ed.putInt(key, value)
            isSuccess = ed.commit()
        }
        return isSuccess
    }

    fun putBoolean(
        key: String?,
        file: String,
        value: Boolean
    ): Boolean {
        var isSuccess = false
        val ed = getEditor(file)
        if (null != ed) {
            ed.putBoolean(key, value)
            isSuccess = ed.commit()
        }
        return isSuccess
    }

    fun putStringSet(
        key: String?,
        file: String,
        value: Set<String?>?
    ): Boolean {
        var isSuccess = false
        val ed = getEditor(file)
        if (null != ed) {
            ed.putStringSet(key, value)
            isSuccess = ed.commit()
        }
        return isSuccess
    }

    fun putStringArray(
        key: String?,
        file: String,
        value: ArrayList<String?>
    ): Boolean {
        var isSuccess = false
        val ed = getEditor(file)
        if (null != ed) {
            val a = JSONArray()
            for (i in value.indices) {
                a.put(value[i])
            }
            if (!value.isEmpty()) {
                ed.putString(key, a.toString())
            } else {
                ed.putString(key, null)
            }
            isSuccess = ed.commit()
        }
        return isSuccess
    }

    //endregion setter
    //region getter
    fun getString(
        key: String?,
        file: String,
        defValue: String?
    ): String? {
        var str = defValue
        val pref = getPref(file)
        if (null != pref) {
            str = pref.getString(key, defValue)
        }
        return str
    }

    fun getLong(key: String?, file: String, defValue: Long): Long {
        var tInt = defValue
        val pref = getPref(file)
        if (null != pref) {
            tInt = pref.getLong(key, defValue)
        }
        return tInt
    }

    fun getInt(key: String?, file: String, defValue: Int): Int {
        var tInt = defValue
        val pref = getPref(file)
        if (null != pref) {
            tInt = pref.getInt(key, defValue)
        }
        return tInt
    }

    fun getBoolean(
        key: String?,
        file: String,
        defValue: Boolean
    ): Boolean {
        var tBoolean = defValue
        val pref = getPref(file)
        if (null != pref) {
            tBoolean = pref.getBoolean(key, defValue)
        }
        return tBoolean
    }

    fun getStringSet(
        key: String?,
        file: String,
        defValue: Set<String?>?
    ): Set<String?>? {
        var tSet = defValue
        val pref = getPref(file)
        if (null != pref) {
            tSet = pref.getStringSet(key, defValue)
        }
        return tSet
    }

    fun getStringArray(
        key: String?,
        file: String,
        defValue: ArrayList<String?>?
    ): ArrayList<String?>? {
        var tArray = defValue
        val pref = getPref(file)
        if (null != pref) {
            try {
                val json = pref.getString(key, null)
                if (json != null) {
                    val temp =
                        ArrayList<String?>()
                    try {
                        val a = JSONArray(json)
                        for (i in 0 until a.length()) {
                            val s = a.optString(i)
                            temp.add(s)
                        }
                        tArray = temp
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return tArray
    }

    //endregion getter
    fun clearAll(file: String): Boolean {
        var isSuccess = false
        val ed = getEditor(file)
        if (null != ed) {
            isSuccess = ed.clear().commit()
        }
        return isSuccess
    }

    companion object {
        val instance: MyStorage by lazy {
            MyStorage()
        }
    }
}