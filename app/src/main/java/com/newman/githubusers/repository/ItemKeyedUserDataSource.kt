package com.newman.githubusers.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.ItemKeyedDataSource
import com.newman.githubusers.model.data.User
import com.newman.githubusers.network.manager.github.service.GithubApi
import com.newman.githubusers.network.manager.github.util.GithubApiResponse
import com.newman.githubusers.repository.list.NetworkState
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.ExecutorService

class ItemKeyedUserDataSource(
    private val githubApi: GithubApi,
    private val queryString: String,
    private val retryExecutor: ExecutorService
) : ItemKeyedDataSource<Int, User>() {
    // keep a function reference for the retry event
    private var retry: (() -> Any)? = null
    private var pageIndex = 1

    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()
    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            retryExecutor.execute {
                it.invoke()
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<User>) {
        // ignored, since we only ever append to our initial load
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<User>) {
        // set network value to loading.
        networkState.postValue(NetworkState.LOADING)
        // even though we are using async retrofit API here, we could also use sync
        // it is just different to show that the callback can be called async.

        githubApi.searchUser(queryString, pageIndex).enqueue(
            object : retrofit2.Callback<GithubApiResponse<ArrayList<User>>> {
                override fun onFailure(
                    call: Call<GithubApiResponse<ArrayList<User>>>,
                    t: Throwable
                ) {
                    // keep a lambda for future retry
                    retry = {
                        loadAfter(params, callback)
                    }
                    // publish the error
                    networkState.postValue(NetworkState.error(t.message ?: "unknown err"))
                }

                override fun onResponse(
                    call: Call<GithubApiResponse<ArrayList<User>>>,
                    response: Response<GithubApiResponse<ArrayList<User>>>
                ) {
                    if (response.isSuccessful) {
                        pageIndex++
                        val items = response.body()?.items ?: arrayListOf()
                        // clear retry since last request succeeded
                        retry = null
                        callback.onResult(items)
                        networkState.postValue(NetworkState.LOADED)
                    } else {
                        retry = {
                            loadAfter(params, callback)
                        }
                        networkState.postValue(
                            NetworkState.error("error code: ${response.code()}")
                        )
                    }
                }
            })
    }

    override fun getKey(item: User): Int = item.id

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<User>
    ) {
        if (queryString.isBlank()) {
            val items = arrayListOf<User>()
            retry = null
            networkState.postValue(NetworkState.LOADED)
            initialLoad.postValue(NetworkState.LOADED)
            callback.onResult(items)
        } else {
            val request = githubApi.searchUser(queryString, pageIndex)
            // update network states.
            // we also provide an initial load state to the listeners so that the UI can know when the
            // very first list is loaded.
            networkState.postValue(NetworkState.LOADING)
            initialLoad.postValue(NetworkState.LOADING)

            // triggered by a refresh, we better execute sync
            try {
                val response = request.execute()
                if (response.isSuccessful) {
                    pageIndex++
                }
                val items = response.body()?.items ?: arrayListOf()
                retry = null
                networkState.postValue(NetworkState.LOADED)
                initialLoad.postValue(NetworkState.LOADED)
                callback.onResult(items)
            } catch (ioException: IOException) {
                retry = {
                    loadInitial(params, callback)
                }
                val error = NetworkState.error(ioException.message ?: "unknown error")
                networkState.postValue(error)
                initialLoad.postValue(error)
            }
        }
    }
}