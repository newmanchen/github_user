/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.newman.githubusers.repository

import androidx.annotation.MainThread
import androidx.paging.PagedList
import com.newman.githubusers.common.util.MyStorage
import com.newman.githubusers.helper.PagingRequestHelper
import com.newman.githubusers.helper.createStatusLiveData
import com.newman.githubusers.model.data.User
import com.newman.githubusers.network.manager.github.service.GithubApi
import com.newman.githubusers.network.manager.github.util.GithubApiResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

/**
 * This boundary callback gets notified when user reaches to the edges of the list such that the
 * database cannot provide any more data.
 * <p>
 * The boundary callback might be called multiple times for the same direction so it does its own
 * rate limiting using the PagingRequestHelper class.
 */
class UserBoundaryCallback(
    private val githubApi: GithubApi,
    private val queryString: String,
    private val handleResponse: (ArrayList<User>) -> Unit,
    private val ioExecutor: Executor
) : PagedList.BoundaryCallback<User>() {

    companion object {
        const val FILE_NAME = "keys.for.api"
        const val KEY_LAST_PAGE_INDEX = "key.last.page.index"
    }

    var pageIndex = 1
    val helper = PagingRequestHelper(ioExecutor)
    val networkState = helper.createStatusLiveData()

    /**
     * Database returned 0 items. We should query the backend for more items.
     */
    @MainThread
    override fun onZeroItemsLoaded() {
        pageIndex = 1
        updatePageInfo()
        helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL) {
            githubApi.searchUser(queryString, pageIndex)
                .enqueue(createWebserviceCallback(it))
        }
    }

    /**
     * User reached to the end of the list.
     */
    @MainThread
    override fun onItemAtEndLoaded(itemAtEnd: User) {
        val page = MyStorage.instance.getInt(KEY_LAST_PAGE_INDEX, FILE_NAME, 1)
        helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) {
            githubApi.searchUser(queryString, page)
                .enqueue(createWebserviceCallback(it))
        }
    }

    override fun onItemAtFrontLoaded(itemAtFront: User) {
        // ignored, since we only ever append to what's in the DB
    }

    private fun createWebserviceCallback(it: PagingRequestHelper.Request.Callback)
            : Callback<GithubApiResponse<ArrayList<User>>> {
        return object : Callback<GithubApiResponse<ArrayList<User>>> {
            override fun onFailure(
                call: Call<GithubApiResponse<ArrayList<User>>>,
                t: Throwable
            ) {
                it.recordFailure(t)
            }

            override fun onResponse(
                call: Call<GithubApiResponse<ArrayList<User>>>,
                response: Response<GithubApiResponse<ArrayList<User>>>
            ) {
                if (response.body()?.items != null) {
//                    response.headers()["Link"]
                    pageIndex++
                    updatePageInfo()
                    ioExecutor.execute {
                        handleResponse(response.body()?.items!!)
                        it.recordSuccess()
                    }
                }
            }
        }
    }

    private fun updatePageInfo() {
        MyStorage.instance.putInt(KEY_LAST_PAGE_INDEX, FILE_NAME, pageIndex)
    }
}