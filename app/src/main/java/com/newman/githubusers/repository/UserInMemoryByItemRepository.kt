package com.newman.githubusers.repository

import androidx.annotation.MainThread
import androidx.lifecycle.switchMap
import androidx.paging.Config
import androidx.paging.toLiveData
import com.newman.githubusers.model.data.User
import com.newman.githubusers.network.manager.github.manager.BaseGithubRequestManager
import com.newman.githubusers.network.manager.github.service.GithubApi
import com.newman.githubusers.repository.list.Listing
import java.util.concurrent.Executors
import javax.inject.Singleton

/**
 * Repository implementation that returns a Listing that loads data directly from the network
 * and uses the Item's name as the key to discover prev/next pages.
 */
@Singleton
class UserInMemoryByItemRepository : GithubUserRepository {
    private val githubApi by lazy {
        BaseGithubRequestManager.getRetrofit().create(GithubApi::class.java)
    }
    private val networkExecutor by lazy {
        Executors.newFixedThreadPool(5)
    }

    private val ioExecutor by lazy {
        Executors.newSingleThreadExecutor()
    }

    @MainThread
    override fun users(queryString: String): Listing<User> {
        val sourceFactory = UserDataSourceFactory(githubApi, queryString, networkExecutor)

        val pageSize = 30
        // We use toLiveData Kotlin ext. function here, you could also use LivePagedListBuilder
        val livePagedList = sourceFactory.toLiveData(
            // we use Config Kotlin ext. function here, could also use PagedList.Config.Builder
            config = Config(
                pageSize = pageSize,
                enablePlaceholders = false,
                initialLoadSizeHint = pageSize * 2
            ),
            // provide custom executor for network requests, otherwise it will default to
            // Arch Components' IO pool which is also used for disk access
            fetchExecutor = networkExecutor
//            boundaryCallback = UserBoundaryCallback(
//                githubApi = githubApi,
//                queryString = queryString,
//                ioExecutor = ioExecutor,
//                handleResponse = {
//                    if (sourceFactory.sourceLiveData.value is ItemKeyedUserDataSource) {
//                        val dataSource = sourceFactory.sourceLiveData.value!!
//                        val tempUsers = dataSource.searchResults[queryString]
//                        if (tempUsers == null) {
//                            dataSource.searchResults[queryString] = it
//                        } else {
//                            tempUsers.addAll(it)
//                        }
//                        sourceFactory.sourceLiveData.postValue(dataSource)
//                    }
//                }
//            )
        )

        val refreshState = sourceFactory.sourceLiveData.switchMap {
            it.initialLoad
        }
        return Listing(
            pagedList = livePagedList,
            networkState = sourceFactory.sourceLiveData.switchMap {
                it.networkState
            },
            retry = {
                sourceFactory.sourceLiveData.value?.retryAllFailed()
            },
            refresh = {
                sourceFactory.sourceLiveData.value?.invalidate()
            },
            refreshState = refreshState
        )
    }
}

