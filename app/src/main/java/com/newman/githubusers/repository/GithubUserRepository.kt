package com.newman.githubusers.repository

import com.newman.githubusers.model.data.User
import com.newman.githubusers.repository.list.Listing

interface GithubUserRepository {
    fun users(queryString: String): Listing<User>
}