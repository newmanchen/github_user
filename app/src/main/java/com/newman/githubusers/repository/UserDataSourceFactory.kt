package com.newman.githubusers.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.newman.githubusers.model.data.User
import com.newman.githubusers.network.manager.github.service.GithubApi
import java.util.concurrent.ExecutorService

/**
 * A simple data source factory which also provides a way to observe the last created data source.
 * This allows us to channel its network request status etc back to the UI. See the Listing creation
 * in the Repository class.
 */
class UserDataSourceFactory(
    private val githubApi: GithubApi,
    private val queryName: String,
    private val retryExecutor: ExecutorService
) : DataSource.Factory<Int, User>() {
    val sourceLiveData = MutableLiveData<ItemKeyedUserDataSource>()
    override fun create(): DataSource<Int, User> {
        val source = ItemKeyedUserDataSource(githubApi, queryName, retryExecutor)
        sourceLiveData.postValue(source)
        return source
    }
}
