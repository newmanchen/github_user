package com.newman.githubusers.network.interceptor

import com.newman.githubusers.common.MyCommonUtil
import com.newman.githubusers.network.exception.NoConnectionException
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.internal.http.HttpHeaders
import java.io.IOException

class MyServerInterceptor : Interceptor {
    private val headerContentType = "Content-Type"
    private val headerAccept = "Accept"

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!MyCommonUtil.isNetworkConnect()) {
            MyCommonUtil.showToast("請檢查網路狀況！")
            throw NoConnectionException(chain.request().url().toString())
        }
        val origin = chain.request()
        val originHeaders = origin.headers()
        val needContentType = originHeaders[headerContentType] == null
        val needAccept = originHeaders[headerAccept] == null
        val builder = origin.newBuilder()

        if (needContentType) {
            builder.addHeader(headerContentType, "application/json")
        }
        if (needAccept) {
            builder.addHeader(headerAccept, "application/json")
        }
        val request = builder.build()
        val response: Response
        response = try {
            chain.proceed(request)
        } catch (ex: Exception) {
            throw ex
        }
        if (response.isSuccessful) {
            val responseBody = response.body()
            if (HttpHeaders.hasBody(response)) {
                val source = responseBody!!.source()
                source.request(Long.MAX_VALUE) // Buffer the entire body.
            }
        }
        return response
    }
}