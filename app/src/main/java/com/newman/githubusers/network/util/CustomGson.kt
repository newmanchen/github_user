package com.newman.githubusers.network.util

import com.google.gson.*
import com.newman.githubusers.BuildConfig
import java.lang.reflect.Type

object CustomGson {
    val customGsonBuilder: GsonBuilder
        get() = GsonBuilder().registerTypeAdapter(
            Number::class.java,
            JsonDeserializer { json: JsonElement?, _: Type?, _: JsonDeserializationContext? ->
                val number = json?.asJsonPrimitive?.asNumber ?: 0
                json?.let {
                    if (it.asJsonPrimitive.isString) {
                        if (!BuildConfig.DEBUG) {
                            try {
                                throw RuntimeException("Workaround for String format number")
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
                number
            } as JsonDeserializer<*>
        ).registerTypeAdapter(
            String::class.java,
            JsonDeserializer { json: JsonElement, _: Type?, _: JsonDeserializationContext? ->
                var resultString = ""
                if (json.isJsonPrimitive) {
                    resultString = json.asJsonPrimitive.asString
                } else {
                    if (!BuildConfig.DEBUG) {
                        try {
                            throw RuntimeException("Workaround for non-primitive String")
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        resultString = Gson().fromJson(json, String::class.java)
                    }
                }
                resultString
            }
        )
            .registerTypeAdapterFactory(GsonTypeAdapterFactory())
}