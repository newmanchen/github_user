package com.newman.githubusers.network.manager.github.util

object GithubConstant {

    // GET https://api.github.com/search/users?q=newman
    const val API_SEARCH_USER = "/search/users"
}