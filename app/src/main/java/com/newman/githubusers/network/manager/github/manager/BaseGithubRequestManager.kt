package com.newman.githubusers.network.manager.github.manager

import com.newman.githubusers.network.NetworkConstant
import com.newman.githubusers.network.interceptor.MyServerInterceptor
import com.newman.githubusers.network.manager.MyRequestManager
import com.newman.githubusers.network.util.CustomGson
import retrofit2.Retrofit

open class BaseGithubRequestManager : MyRequestManager() {
    companion object {
        fun getRetrofit(): Retrofit {
            return getRetrofit(
                NetworkConstant.SERVER_GITHUB, MyServerInterceptor(),
                CustomGson.customGsonBuilder.create()
            )
        }
    }
}