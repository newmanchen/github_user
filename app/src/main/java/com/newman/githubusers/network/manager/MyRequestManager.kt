package com.newman.githubusers.network.manager

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.newman.githubusers.BuildConfig
import com.newman.githubusers.network.interceptor.MyServerInterceptor
import de.greenrobot.event.EventBus
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

open class MyRequestManager : EventBus() {
    private var nextTaskID = 0
    var taskIDs: MutableMap<String, Int> =
        ConcurrentHashMap()

    fun updateApiTaskID(api: String, replace: Boolean): Int {
        synchronized(this) {
            ++nextTaskID
            if (nextTaskID >= Short.MAX_VALUE) {
                nextTaskID = 1
            }
            if (!replace) {
                taskIDs[api]?.let {
                    return -it
                }
            }
        }
        taskIDs[api] = nextTaskID
        return nextTaskID
    }

    fun containsApiTaskID(api: String, taskID: Int): Boolean {
        var ret = false
        taskIDs[api]?.let {
            ret = it == taskID
        }
        return ret
    }

    fun clearApiTaskID(api: String?) {
        if (api == null) {
            return
        }
        taskIDs.remove(api)
    }

    companion object {
        private lateinit var okHttpClientInstance: OkHttpClient

        fun getRetrofit(url: String): Retrofit {
            return getRetrofit(url, null, null)
        }

        fun getRetrofit(url: String, interceptor: MyServerInterceptor?, gson: Gson?): Retrofit {
            return Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(if (gson != null) GsonConverterFactory.create(gson) else GsonConverterFactory.create())
                .client(getNewOkHttpClient(interceptor))
                .build()
        }

        private fun getNewOkHttpClient(
            interceptor: MyServerInterceptor? = null
        ): OkHttpClient {
            val builder = OkHttpClient.Builder()
            builder.connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)

            if (interceptor == null) {
                builder.addInterceptor(MyServerInterceptor())
            } else {
                builder.addInterceptor(interceptor)
            }

            if (BuildConfig.DEBUG) {
                builder.addNetworkInterceptor(StethoInterceptor())
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            }

            val specBuilder =
                ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
            specBuilder.tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                    CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,  //
                    CipherSuite.TLS_RSA_WITH_AES_256_GCM_SHA384,
                    CipherSuite.TLS_RSA_WITH_AES_128_GCM_SHA256,
                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,
                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
                    CipherSuite.TLS_RSA_WITH_3DES_EDE_CBC_SHA,
                    CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA,
                    CipherSuite.TLS_RSA_WITH_AES_256_CBC_SHA,
                    CipherSuite.TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA,
                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                    CipherSuite.TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA,
                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA
                )
            builder.connectionSpecs(
                listOf(
                    specBuilder.build(),
                    ConnectionSpec.CLEARTEXT
                )
            )
            //            builder.sslSocketFactory()

            // set max request in the same time
            val dispatcher =
                Dispatcher(Executors.newFixedThreadPool(10))
            dispatcher.maxRequests = 10
            dispatcher.maxRequestsPerHost = 10
            builder.dispatcher(dispatcher)
            okHttpClientInstance = builder.build()
            return okHttpClientInstance
        }
    }
}