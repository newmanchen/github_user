package com.newman.githubusers.network.callback

import com.newman.githubusers.network.manager.MyRequestManager
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class MyWebCallback<T>(manager: MyRequestManager, logKey: String) : Callback<T> {
    companion object {
        private const val TAG = "REST-API"
    }

    private val taskKey: String
    private val taskID: Int
    private val requestManager: MyRequestManager?

    init {
        requestManager = manager
        taskKey = logKey
        taskID = manager.updateApiTaskID(taskKey, false)
    }

    abstract fun onSuccess(response: T)
    abstract fun onError(errorCode: Int, t: Throwable?)

    protected fun preSuccess(result: T?): T? {
        return result
    }

    override fun onResponse(
        call: Call<T>,
        response: Response<T>
    ) {
        if (taskID > 0 && requestManager != null) {
            if (!requestManager.containsApiTaskID(taskKey, taskID)) {
                return
            }
        }
        try {
            if (response.isSuccessful) {
                var result = response.body()
                result = preSuccess(result)
                result?.let { onSuccess(it) }
            } else {
                val errorBody = response.errorBody()!!.string()
                val json = JSONObject(errorBody)
                // if check specific error status code that need to refresh token, do it here
                var errorStatusCode = json.optInt("statusCode")
                if (errorStatusCode == 0) {
                    errorStatusCode = json.optInt("status_code")
                }
                onError(errorStatusCode, Throwable(json.optString("message")))
            }
        } catch (e: Exception) {
            try {
                onFailure(call, Throwable(e.message + ""))
            } catch (e1: Exception) {
            }
        }
        requestManager?.clearApiTaskID(taskKey)
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        try {
            onError(54389999, t)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getMessage(t: Throwable?): String? {
        return t?.message
    }
}