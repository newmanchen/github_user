package com.newman.githubusers.network.callback

interface ApiCallback<T> {
    fun onSuccess(response: T)
    fun onFail(e: Throwable?)
}