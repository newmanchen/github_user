package com.newman.githubusers.network.manager.github.util

import com.google.gson.annotations.SerializedName

data class GithubApiResponse<T>(
    @SerializedName("total_count") var total_count: Int? = 0,
    @SerializedName("incomplete_results") var incomplete_results: Boolean? = false,
    @SerializedName("items") var items: T? = null
)
