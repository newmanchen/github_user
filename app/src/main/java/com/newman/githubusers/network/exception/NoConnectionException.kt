package com.newman.githubusers.network.exception

import java.io.IOException

class NoConnectionException(message: String?) : IOException(message)