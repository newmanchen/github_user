package com.newman.githubusers.network.util

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.IOException

class GsonTypeAdapterFactory : TypeAdapterFactory {
    override fun <T> create(gson: Gson, type: TypeToken<T>): TypeAdapter<T?> {
        val adapter = gson.getDelegateAdapter(this, type)
        return object : TypeAdapter<T?>() {
            @Throws(IOException::class)
            override fun write(
                out: JsonWriter,
                value: T?
            ) {
                adapter.write(out, value)
            }

            @Throws(IOException::class)
            override fun read(input: JsonReader): T? {
                return try {
                    adapter.read(input)
                } catch (e: Throwable) {
                    consumeAll(input)
                    null
                }
            }

            @Throws(IOException::class)
            private fun consumeAll(input: JsonReader) {
                if (input.hasNext()) {
                    when (input.peek()) {
                        JsonToken.STRING -> {
                            input.nextString()
                        }
                        JsonToken.BEGIN_ARRAY -> {
                            input.beginArray()
                            consumeAll(input)
                            input.endArray()
                        }
                        JsonToken.BEGIN_OBJECT -> {
                            input.beginObject()
                            consumeAll(input)
                            input.endObject()
                        }
                        JsonToken.END_ARRAY -> {
                            input.endArray()
                        }
                        JsonToken.END_OBJECT -> {
                            input.endObject()
                        }
                        JsonToken.NUMBER -> {
                            input.nextString()
                        }
                        JsonToken.BOOLEAN -> {
                            input.nextBoolean()
                        }
                        JsonToken.NAME -> {
                            input.nextName()
                            consumeAll(input)
                        }
                        JsonToken.NULL -> {
                            input.nextNull()
                        }
                        else -> {
                        }
                    }
                }
            }
        }
    }
}