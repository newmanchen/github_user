package com.newman.githubusers.network.manager.github.manager

import com.newman.githubusers.model.data.User
import com.newman.githubusers.network.manager.github.service.GithubApi
import com.newman.githubusers.network.manager.github.util.GithubApiResponse

class SearchManager : BaseGithubRequestManager() {
    private val searchService = getRetrofit().create(GithubApi::class.java)

    suspend fun searchUserSync(q: String): GithubApiResponse<List<User>> {
        return try {
            searchService.searchUserSync(q)
        } catch (e: Exception) {
            GithubApiResponse()
        }
    }
}