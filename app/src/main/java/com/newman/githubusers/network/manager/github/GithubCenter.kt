package com.newman.githubusers.network.manager.github

import com.newman.githubusers.network.manager.github.manager.SearchManager

class GithubCenter {
    val searchManager: SearchManager = SearchManager()
}
