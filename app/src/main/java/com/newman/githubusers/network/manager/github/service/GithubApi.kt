package com.newman.githubusers.network.manager.github.service

import com.newman.githubusers.model.data.User
import com.newman.githubusers.network.NetworkConstant
import com.newman.githubusers.network.manager.github.util.GithubApiResponse
import com.newman.githubusers.network.manager.github.util.GithubConstant
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {
    @GET(GithubConstant.API_SEARCH_USER)
    fun searchUser(
        @Query(NetworkConstant.KEY_Q) q: String,
        @Query(NetworkConstant.KEY_PAGE) page: Int = 1
    ): Call<GithubApiResponse<ArrayList<User>>>

    @GET(GithubConstant.API_SEARCH_USER)
    suspend fun searchUserSync(@Query(NetworkConstant.KEY_Q) q: String): GithubApiResponse<List<User>>
}