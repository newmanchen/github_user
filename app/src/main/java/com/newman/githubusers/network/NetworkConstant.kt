package com.newman.githubusers.network

import com.newman.githubusers.BuildConfig

object NetworkConstant {
    const val SERVER_GITHUB = BuildConfig.githubServerUrl

    const val KEY_Q = "q"
    const val KEY_PAGE = "page"
}