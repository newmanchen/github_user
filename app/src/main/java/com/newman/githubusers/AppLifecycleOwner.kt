package com.newman.githubusers

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.newman.githubusers.common.util.MyLog

class AppLifecycleOwner : LifecycleObserver {
    private val TAG = "AppLifecycleOwner"

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() { // app moved to foreground
        MyLog.d(TAG, "onMoveToForeground()")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() { // app moved to background
        MyLog.d(TAG, "onMoveToBackground()")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onAppDestroy() {
        MyLog.d(TAG, "onAppDestroy()")
    }
}